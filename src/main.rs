fn main() {
    let arr1 = [100, 1, 2, 3, 4, 12, 6, 600];
    let arr2 = [7, 12, 11, 3, 5, 1, 8, 2];

    let result = reconcileHelper(&arr1, &arr2);

    println!("Numbers in array 1 that aren't in array2:");
    println!("{:?}", result.0);
    println!("Numbers in array 2 that aren't in array1:");
    println!("{:?}", result.1);
}

fn reconcileHelper(arr1: &[usize], arr2: &[usize]) -> (Vec<usize>, Vec<usize>) {
    let res1 = arr_iter_compare_spd(arr1, arr2);
    let res2 = arr_iter_compare_spd(arr2, arr1);

    (res1, res2)
}

fn arr_iter_compare_spd(arr1: &[usize], arr2: &[usize]) -> Vec<usize> {
    let mut result = vec!();

    if arr2.len() > 0 {
        let mut sort_2 = arr2.to_vec();
        sort_2.sort();
        for i in arr1 {
            if !b_search_contains(*i, sort_2.to_vec()) {
                result.push(*i);
            }
        }
    } else {
        result = arr1.to_vec();
    }

    result
}

// return true if match false if not
// Standard library does include a binary search function on vectors...
fn b_search_contains(item: usize, arr: Vec<usize>) -> bool {
    if arr.len() > 0 && item <= arr[arr.len()-1] {
        // Once a set reaches a magic point iteration become faster. We will put the magic point at 4, but I think
        // some testing I had read put the number around 20. However I suspect it is language, platform, and
        // data specific so benchmarking and testing would be required to find this number.
        if arr.len() < 4 {
            return arr_contains(item, arr.as_slice());
        }

        // Going to cut the array in half at each comparision for a
        // binary search.
        let test_idx = arr.len() / 2;
        let test_item = arr[test_idx];

        if item > test_item {
            let part = arr[test_idx..arr.len()].to_vec();
            return b_search_contains(item, part.to_vec());
        } else {
            let part = arr[0..test_idx].to_vec();
            return b_search_contains(item, part.to_vec());
        }
    }
    false
}

fn arr_contains(item: usize, arr: &[usize]) -> bool {
    for i in arr {
        if item == *i {
            return true;
        }
    }
    false
}

#[test]
fn test1() {
    let arr1 = [6, 1, 2, 3, 60, 5, 0];
    let arr2 = [5, 9, 7, 8];

    let result = reconcileHelper(&arr1, &arr2);
    let mut ord0 = result.0;
    ord0.sort();
    let mut ord1 = result.1;
    ord1.sort();

    assert!(ord0.as_slice() == [0, 1, 2, 3, 6, 60] && ord1.as_slice() == [7, 8, 9])
}

#[test]
fn test2() {
    let arr1 = [];
    let arr2 = [2, 9, 3, 1, 0];
    let result = reconcileHelper(&arr1, &arr2);

    let mut ord0 = result.0;
    ord0.sort();
    let mut ord1 = result.1;
    ord1.sort();

    assert!(ord1.as_slice() == [0, 1, 2, 3, 9] && ord0.len() == 0)
}